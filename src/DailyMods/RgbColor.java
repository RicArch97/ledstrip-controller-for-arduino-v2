package DailyMods;

public class RgbColor {
	
	private String colorName;
	private int R, G, B;

	public RgbColor(String colorName, int R, int G, int B) {
	        this.colorName = colorName;
	        this.R = R;
	        this.G = G;
	        this.B = B;
	    }

	public String getColorname() {
		return this.colorName;
	}

	public int getR() {
		return this.R;
	}
	
	public int getG() {
		return this.G;
	}
	
	public int getB() {
		return this.B;
	}

	public void setColorname(String colorName) {
		this.colorName = colorName;
	}

	public void setR(int R) {
		this.R = R;
	}
	
	public void setG(int G) {
		this.G = G;
	}
	
	public void setB(int B) {
		this.B = B;
	}
}
