package DailyMods;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.OutputStream;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class LedstripSettings extends JPanel {
	public OutputStream output;

	public LedstripSettings(OutputStream output) {
		this.output = output;
		
		this.setBackground(new Color(240, 240, 240));
		this.setBounds(100, 120, 300, 500);
		this.setLayout(new BorderLayout());
		
		JPanel settings = new JPanel();
		settings.setBackground(new Color(240, 240, 240));
		settings.setLayout(new BoxLayout(settings, BoxLayout.Y_AXIS));
		this.add(settings, BorderLayout.CENTER);
		
		JPanel settings2 = new JPanel();
		settings2.setBackground(new Color(240, 240, 240));
		settings2.setLayout(new BoxLayout(settings2, BoxLayout.Y_AXIS));
		settings.add(settings2);
		
		JPanel title = new JPanel();
		title.setBackground(new Color(240, 240, 240));
		title.setLayout(new FlowLayout(FlowLayout.CENTER));
		settings2.add(title);
		
		JLabel text = new JLabel("Ledstrip Settings", SwingConstants.CENTER);
		text.setFont(new Font("Poor Richard", Font.PLAIN, 30));
		title.add(text);
		
		JPanel ledTextPanel = new JPanel();
		ledTextPanel.setBackground(new Color(240, 240, 240));
		ledTextPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		settings2.add(ledTextPanel);
		
		JLabel ledAmountText = new JLabel("Amount of leds:");
		ledAmountText.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		ledTextPanel.add(ledAmountText);
		
		JPanel leds = new JPanel();
		leds.setBackground(new Color(240, 240, 240));
		leds.setLayout(new GridBagLayout());
		settings2.add(leds);
		
		GridBagConstraints led = new GridBagConstraints();
		led.weightx = 1.0;
		led.anchor = GridBagConstraints.NORTH;
		
		HintTextField ledAmount = new HintTextField("Leds:");
		ledAmount.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		ledAmount.setPreferredSize(new Dimension(70, 30));
		led.gridx = 0;
		leds.add(ledAmount, led);

		JButton setAmount = new JButton("OK");
		setAmount.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		setAmount.setSize(new Dimension(70, 30));
		led.gridx = 1;
		leds.add(setAmount, led);
		
		JPanel speedTextPanel = new JPanel();
		speedTextPanel.setBackground(new Color(240, 240, 240));
		speedTextPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		settings2.add(speedTextPanel);
		
		JLabel speedText = new JLabel("Speed:", SwingConstants.CENTER);
		speedText.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		speedTextPanel.add(speedText);
		
		JPanel speed = new JPanel();
		speed.setBackground(new Color(240, 240, 240));
		speed.setLayout(new GridBagLayout());
		settings2.add(speed);
		
		GridBagConstraints spe = new GridBagConstraints();
		spe.weightx = 1.0;
		spe.anchor = GridBagConstraints.NORTH;
		
		ChangeSpeed speedAdjust = new ChangeSpeed();
		spe.gridx = 0;
		speed.add(speedAdjust, spe);
		
		JButton setSpeed = new JButton("OK");
		setSpeed.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		setSpeed.setSize(new Dimension(70, 30));
		spe.gridx = 1;
		speed.add(setSpeed, spe);
		
		JPanel currentSettings = new JPanel();
		currentSettings.setBackground(new Color(240, 240, 240));
		currentSettings.setLayout(new FlowLayout(FlowLayout.CENTER));
		settings.add(currentSettings);
		
		JLabel currentAmount = new JLabel("Current amount of leds:", SwingConstants.CENTER);
		currentAmount.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		currentSettings.add(currentAmount);
		
		JLabel ledNumber = new JLabel("60");
		ledNumber.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		currentSettings.add(ledNumber);

		setAmount.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int amount = Integer.parseInt(ledAmount.getText());
				ledNumber.setText(ledAmount.getText());
				sendingData((byte) 201);
				sendingData((byte) amount);
			}
		});
		
		setSpeed.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int factor = speedAdjust.getSpeedValue();
				double speedValue = factor * 25.5;
				sendingData((byte) 200);
				sendingData((byte) speedValue);
			}
		});
		
		
	}

	public void sendingData(byte value) {
		try {
			output.write(value);
			output.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
