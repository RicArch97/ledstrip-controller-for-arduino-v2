package DailyMods;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.TabbedPaneUI;
import javax.swing.plaf.basic.BasicTabbedPaneUI;
import com.fazecast.jSerialComm.SerialPort;

public class MainGUI {
	JFrame frame;
	JTabbedPane tabbedPane;
	ReadCOM comm;
	Point mouseCoords = null;
	Font font;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainGUI window = new MainGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MainGUI() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(550, 150, 1500, 768);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setContentPane(new MyPanel());
		frame.getContentPane().setLayout(new BorderLayout());
		windowDesign();
		tabHeaderDesign();
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setOpaque(false);
		tabbedPane.setBackground(new Color(204,204,204,32));
		frame.getContentPane().add(tabbedPane, BorderLayout.CENTER);
		
		try {
		     GraphicsEnvironment ge = 
		         GraphicsEnvironment.getLocalGraphicsEnvironment();
		     ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("Fonts/TomeOfTheUnknown.ttf")));
		     font = Font.createFont(Font.TRUETYPE_FONT, new File("Fonts/TomeOfTheUnknown.ttf"));
		} catch (IOException|FontFormatException e) {
			e.printStackTrace();
		}
		
		HomeTab home = new HomeTab(tabbedPane, font);

		comm = new ReadCOM();
		comm.readCOM(tabbedPane, frame, font);
		
		ProcessThread thread = new ProcessThread(comm, tabbedPane);
		thread.start();
	}
	
	public void windowDesign() 
	{
		final JPanel windowBar = new JPanel();
        final JButton minimize = new JButton("\u2796");
        final JButton exit = new JButton("\u2716");
        final JButton maximize = new JButton("\u25A1");
        windowBar.setLayout(new FlowLayout(FlowLayout.RIGHT));
        
        exit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        exit.setOpaque(false);
        exit.setContentAreaFilled(false);
        exit.setBorderPainted(false);
        exit.setForeground(Color.WHITE);
        exit.setFocusPainted(false);
        
        minimize.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                frame.setState(JFrame.ICONIFIED);
            }
        });
        minimize.setOpaque(false);
        minimize.setContentAreaFilled(false);
        minimize.setBorderPainted(false);
        minimize.setForeground(Color.WHITE);
        minimize.setFocusPainted(false);
        
        
        maximize.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		if (frame.getExtendedState() == JFrame.NORMAL) {
        			frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        		}
        		
        		else {
        			frame.setExtendedState(JFrame.NORMAL);
        		}
        	}
        });
        maximize.setOpaque(false);
        maximize.setContentAreaFilled(false);
        maximize.setBorderPainted(false);
        maximize.setForeground(Color.WHITE);
        maximize.setFocusPainted(false);
        
        
        windowBar.addMouseListener(new MouseAdapter() {
            public void mouseReleased(MouseEvent e) {
                mouseCoords = null;
            }

            public void mousePressed(MouseEvent e) {
                mouseCoords = e.getPoint();
            }
        });
        
        windowBar.addMouseMotionListener(new MouseMotionListener() {
            public void mouseMoved(MouseEvent e) {
            }

            public void mouseDragged(MouseEvent e) {
                Point currCoords = e.getLocationOnScreen();
                frame.setLocation(currCoords.x - mouseCoords.x, currCoords.y - mouseCoords.y);
            }
        });
        
        windowBar.add(minimize);
        windowBar.add(maximize);
        windowBar.add(exit);
        windowBar.setOpaque(false);
        frame.add(windowBar, BorderLayout.NORTH);
        frame.setLocationRelativeTo(null);
        frame.setUndecorated(true);
	}
	
	public void tabHeaderDesign()
	{
		UIManager.put("TabbedPane.selected", new Color(250,250,250));
		Insets insets = UIManager.getInsets("TabbedPane.contentBorderInsets");
		insets.top = -1;
		insets.left = -1;
		insets.right = -1;
		insets.bottom = -1;
		UIManager.put("TabbedPane.contentBorderInsets", insets);
		UIManager.put("TabbedPane.borderHightlightColor", new Color(204,204,204,32)); 
	    UIManager.put("TabbedPane.darkShadow", new Color(204,204,204,32)); 
	    UIManager.put("TabbedPane.light", new Color(204,204,204,32));
	    UIManager.put("TabbedPane.selectHighlight", new Color(204,204,204,32));
	    UIManager.put("TabbedPane.darkShadow", new Color(204,204,204,32));
	    UIManager.put("TabbedPane.focus", new Color(204,204,204,32));
	    Insets insets1 = UIManager.getInsets("TabbedPane.tabAreaInsets");
	    insets1.left = -2;
	    UIManager.put("TabbedPane.tabAreaInsets", insets1);
	    UIManager.put("OptionPane.messageFont", new Font("Poor Richard", Font.PLAIN, 20));
	    UIManager.put("OptionPane.buttonFont", new Font("Poor Richard", Font.PLAIN, 18));
	}
}
