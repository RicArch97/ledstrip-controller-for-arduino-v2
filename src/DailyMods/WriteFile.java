package DailyMods;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class WriteFile {

	private final String FILENAME;

	public WriteFile(String FILENAME) {
		this.FILENAME = FILENAME;
	}

	public void writeFile(RgbColor color) {

		BufferedWriter bw = null;
		FileWriter fw = null;

		try {
			
			fw = new FileWriter(FILENAME, true);
			bw = new BufferedWriter(fw);
			
			bw.write(color.getColorname() + ",");
			bw.write(color.getR() + ",");
			bw.write(color.getG() + ",");
			bw.write(color.getB() + "\n");

		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {
				
				if (bw != null)
					bw.close();
				
				if (fw != null)
					fw.close();
				
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
}
