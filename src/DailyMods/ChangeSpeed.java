package DailyMods;

import java.util.Hashtable;
import javax.swing.DefaultBoundedRangeModel;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class ChangeSpeed extends JSlider {
	static final int SPEED_MIN = 0;
	static final int SPEED_MAX = 10;
	private int speedValue = 1;

	public ChangeSpeed() {
		DefaultBoundedRangeModel model = new DefaultBoundedRangeModel(1, 0, SPEED_MIN, SPEED_MAX);
		this.setModel(model);

		this.setMajorTickSpacing(1);
		this.setPaintTicks(true);
		
		this.setPaintLabels(true);
        
		// Add positions label in the slider
		Hashtable<Integer, JLabel> position = new Hashtable<Integer, JLabel>();
		position.put(0, new JLabel("0"));
		position.put(1, new JLabel("1"));
		position.put(2, new JLabel("2"));
		position.put(3, new JLabel("3"));
		position.put(4, new JLabel("4"));
		position.put(5, new JLabel("5"));
		position.put(6, new JLabel("6"));
		position.put(7, new JLabel("7"));
		position.put(8, new JLabel("8"));
		position.put(9, new JLabel("9"));
		position.put(10, new JLabel("10"));
		         
		// Set the label to be drawn
		this.setLabelTable(position);
		
		 // Add change listener to the slider
        this.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                speedValue = ((JSlider)e.getSource()).getValue();
            }
        });
	}
	
	public int getSpeedValue() {
		return speedValue;
	}
}
