package DailyMods;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTabbedPane;

public class NewTab {
	String deviceName = "";
	JLabel tab;
	
	public void tabLabels(JTabbedPane tabbedPane, int counter, ArrayList<Devices> dev, JFrame frame, Font font)
	{
		deviceName = "ledstrip" + Integer.toString(counter);
		EffectsTab arduino = new EffectsTab();
		arduino.newTab(tabbedPane, dev, counter, frame);
		tab = new JLabel();
		tab.setText(deviceName);
		tab.setForeground(Color.BLACK);
		tab.setFont(font.deriveFont(Font.PLAIN, 50f));
		tabbedPane.setTabComponentAt(counter, tab);
	}
}
