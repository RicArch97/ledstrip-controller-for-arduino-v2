package DailyMods;

import java.awt.*;
import java.io.*;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.swing.*;
import com.fazecast.jSerialComm.*;

public class ReadCOM {
	ArrayList<Devices> dev;
	int initialLength = 0;
	int portCounter = 1;
	JFrame frame;
	Font font;

	public void readCOM(JTabbedPane tabbedPane, JFrame frame, Font font) {
		this.frame = frame;
		this.font = font;
		
		SerialPort arduinoPorts = null;
		initialLength = SerialPort.getCommPorts().length;
		SerialPort serialPorts[] = new SerialPort[initialLength];
		serialPorts = SerialPort.getCommPorts();
		dev = new ArrayList<Devices>();

		for (int i = 0; i < initialLength; i++) {
			String portName = serialPorts[i].getDescriptivePortName();
			String comName = serialPorts[i].getSystemPortName();

			if (portName.contains("Arduino Mega 2560") || portName.contains("Arduino Uno") || portName.contains("USB-SERIAL CH340")) {
				arduinoPorts = serialPorts[i];
				arduinoPorts.openPort();
				System.out.println("connected to: " + portName);
				
				BufferedReader input = new BufferedReader(new InputStreamReader(arduinoPorts.getInputStream()));
				OutputStream output = arduinoPorts.getOutputStream(); 
				dev.add(new Devices(input, output, portCounter, portName, comName, arduinoPorts));
				
				try {
					Thread.sleep(2500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				NewTab tab = new NewTab();
				tab.tabLabels(tabbedPane, portCounter, dev, frame, font);
				portCounter++;
			}
		}
	}

	public void checkDevices(JTabbedPane tabbedPane) {
		ArrayList<String> COMports = new ArrayList<String>();
		SerialPort arduinoPorts = null;
		
		int len = SerialPort.getCommPorts().length;
		
		for (int j = 0; j < dev.size(); j++) {
			COMports.add(dev.get(j).getComName());
		}
		
		if (initialLength < len) {

			SerialPort serialPorts[] = new SerialPort[len];
			serialPorts = SerialPort.getCommPorts();

			for (int i = 0; i < len; i++) {
				String portName = serialPorts[i].getDescriptivePortName();
				String comName = serialPorts[i].getSystemPortName();

				if (COMports.contains(comName)) {}

				else {
					if (portName.contains("Arduino Mega 2560") || portName.contains("Arduino Uno") || portName.contains("USB-SERIAL CH340")) {
						arduinoPorts = serialPorts[i];
						arduinoPorts.openPort();
						System.out.println("connected to: " + portName);
						
						BufferedReader input = new BufferedReader(new InputStreamReader(arduinoPorts.getInputStream()));
						OutputStream output = arduinoPorts.getOutputStream();
						dev.add(new Devices(input, output, portCounter, portName, comName, arduinoPorts));
						
						try {
							Thread.sleep(2500);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}

						NewTab tab = new NewTab();
						tab.tabLabels(tabbedPane, portCounter, dev, frame, font);
						portCounter++;
					}
				}
			}
			initialLength = len;
		}

		else if (initialLength > len) {
			ArrayList<String> COMportsDisconn = new ArrayList<String>();
			SerialPort serialPorts[] = new SerialPort[len];
			serialPorts = SerialPort.getCommPorts();

			for (int i = 0; i < len; i++) {
				COMportsDisconn.add(serialPorts[i].getSystemPortName());
			}

			COMports.removeAll(COMportsDisconn);

			if (COMports.isEmpty() == false) {
				for (int j = 0; j < dev.size(); j++) {
					if (dev.get(j).getComName().contains(COMports.get(0))) {
						tabbedPane.remove(dev.get(j).getIndex());
						dev.remove(dev.get(j));
						portCounter--;
					}
				}
			}
			// [1 - 2 - 3 - 4] -> [1 - 2 - 4]
			for (int k = 0; k < dev.size() - 1; k++) {
				if ((dev.get(k + 1).getIndex()) - (dev.get(k).getIndex()) == 2) {
					dev.get(k + 1).setIndex(dev.get(k + 1).getIndex() - 1);
				}
			}
			
			// update tabs
			for (int p = 1; p <= dev.size(); p++) {
				String deviceName = "ledstrip" + Integer.toString(p);
				JLabel tab = new JLabel();
				tab.setText(deviceName);
				tab.setForeground(Color.BLACK);
				tab.setFont(font.deriveFont(Font.PLAIN, 50f));
				tabbedPane.setTabComponentAt(p, tab);
			}
			
			initialLength = len;
		}
		
		else if (initialLength == len) {}
	}
}
