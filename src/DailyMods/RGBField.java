package DailyMods;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class RGBField extends JPanel{
	Color color;
	
	@Override
	public void setBackground(Color color) {
		this.color = color;
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(color);
		g.fillRect(0, 0, getWidth(), getHeight());
	}
}
