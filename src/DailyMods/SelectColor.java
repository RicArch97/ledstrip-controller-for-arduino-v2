package DailyMods;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;

public class SelectColor {
	private final String FILENAME = "Save files/Colors.txt";
	private DynamicPanel wave = null;
	private ReadFile colors;
	private Integer[] RGB;
	private JComboBox<Object> combo;
	public Vector<String> colorNames;
	public Vector<ImageIcon> images;
	private Integer[] intArray;
	private ComboBoxRenderer renderer;
	protected int size = 0;
	public int selectedItem = 0;
	public int addColor = 0, deleteColor = 0;
	
	public SelectColor() {
		initialise();
	}

	public SelectColor(DynamicPanel panel) {
		this.wave = panel;
		
		initialise();
	}
	
	public void initialise() {
		colors = new ReadFile(FILENAME);
		colors.readFile();
		size = colors.getColors().size();
		
		colorNames = new Vector<String>();
		images = new Vector<ImageIcon>();
		intArray = new Integer[size];
		RGB = new Integer[3];

		for (int i = 0; i < size; i++) {
			intArray[i] = new Integer(i);

			colorNames.addElement(colors.getColors().get(i).getColorname());
			RGBField field = new RGBField();
			int R = colors.getColors().get(i).getR();
			int G = colors.getColors().get(i).getG();
			int B = colors.getColors().get(i).getB();
			field.setBackground(new Color(R, G, B));
			field.setSize(new Dimension(15, 15));

			images.addElement(new ImageIcon(createImage(field)));
		}

		combo = new JComboBox<Object>(intArray);
		combo.setPreferredSize(new Dimension(150, 40));
		combo.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		
		addColor = size;
		combo.addItem(addColor);

		deleteColor = size + 1;
		combo.addItem(deleteColor);

		renderer = new ComboBoxRenderer();
		renderer.setPreferredSize(new Dimension(150, 30));
		combo.setRenderer(renderer);
		combo.setMaximumRowCount(4);
		
		if (!(size == 0)) {
			getRGB();
			selectedItem = (int) combo.getSelectedItem();
		}

		combo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				getRGB();
				selectedItem = (int) combo.getSelectedItem();
			}
		});
	}

	public void updateOnAdd(RgbColor color) {
		colorNames.addElement(color.getColorname());
		RGBField field = new RGBField();
		int R = color.getR();
		int G = color.getG();
		int B = color.getB();
		field.setBackground(new Color(R, G, B));
		field.setSize(new Dimension(15, 15));
		images.addElement(new ImageIcon(createImage(field)));

		combo.removeItemAt(deleteColor);
		combo.removeItemAt(addColor);

		colors.readFile();
		size = colors.getColors().size();
		combo.addItem(size - 1);

		addColor = size;
		combo.addItem(addColor);

		deleteColor = addColor + 1;
		combo.addItem(deleteColor);

		combo.setSelectedItem(size - 1);
		getRGB();
	}

	public void updateOnDelete(int index) {
		combo.removeAllItems();

		colors.readFile();
		size = colors.getColors().size();

		for (int i = 0; i < size; i++) {
			combo.addItem(i);
		}

		colorNames.removeElementAt(index);
		images.removeElementAt(index);

		addColor = size;
		combo.addItem(addColor);

		deleteColor = addColor + 1;
		combo.addItem(deleteColor);
		getRGB();
	}

	public void getRGB() {
		int index = (int) combo.getSelectedItem();

		for (int i = 0; i < size; i++) {
			if (index == i) {
				RGB[0] = colors.getColors().get(i).getR();
				RGB[1] = colors.getColors().get(i).getG();
				RGB[2] = colors.getColors().get(i).getB();
			}
		}

		if (index == addColor) {
			AddColor add = new AddColor();

			if (add.displayDialog() == true) {
				updateOnAdd(add.getColor());
				if (wave != null) {
					wave.update();
				}
			}

		}

		if (!(size == 0)) {
			if (index == deleteColor) {
				DeleteColor del = new DeleteColor(size, colors);

				if (del.displayDialog() == true) {
					updateOnDelete(del.getIndex());
					if (wave != null) {
						wave.update();
					}
				}
			}
		}
	}

	public BufferedImage createImage(JPanel panel) {

		int w = panel.getWidth();
		int h = panel.getHeight();
		BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = bi.createGraphics();
		panel.print(g);
		g.dispose();
		return bi;
	}

	public Integer[] getValues() {
		return this.RGB;
	}

	public JComboBox<Object> getComboBox() {
		return this.combo;
	}

	public JComboBox<Object> getComboBoxWithoutFunctions() {
		combo.removeItemAt(deleteColor);
		combo.removeItemAt(addColor);

		return this.combo;
	}
	
	public int getSelectedIndex() {
		return this.selectedItem;
	}

	class ComboBoxRenderer extends JLabel implements ListCellRenderer<Object> {

		public ComboBoxRenderer() {
			setOpaque(true);
			setVerticalAlignment(CENTER);
		}

		public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
				boolean cellHasFocus) {

			int selectedIndex = ((Integer) value).intValue();

			if (isSelected) {
				setBackground(list.getSelectionBackground());
				setForeground(list.getSelectionForeground());
			} else {
				setBackground(list.getBackground());
				setForeground(list.getForeground());
			}

			if (selectedIndex < size) {
				ImageIcon icon = images.get(selectedIndex);
				String colorName = colorNames.get(selectedIndex);
				setIcon(icon);
				if (icon != null) {
					setText(colorName);
					setFont(list.getFont());
				}
			}
			if (selectedIndex == addColor) {
				setIcon(null);
				setText("+  Create color");
				setFont(list.getFont());
			}

			if (selectedIndex == deleteColor) {
				setIcon(null);
				setText(" x  Delete color");
				setFont(list.getFont());
			}

			return this;
		}
	}
}
