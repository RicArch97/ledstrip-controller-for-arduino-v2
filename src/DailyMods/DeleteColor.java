package DailyMods;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class DeleteColor {
	private final String FILENAME = "Save files/Colors.txt";
	private ReadFile colors;
	SelectColor color;
	int size = 0;
	
	public DeleteColor(int size, ReadFile colors) {
		this.size = size;
		this.colors = colors;
	}
	
	private JPanel getPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		GridBagConstraints col = new GridBagConstraints();
		
		JLabel name = new JLabel("Select a color you want to delete:");
		name.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		col.gridx = 0;
		col.gridy = 0;
		col.weightx = 1.0;
		col.weighty = 1.0;
		col.insets = new Insets(20, 20, 20, 20);
		panel.add(name, col);
		
		color = new SelectColor();
		col.gridx = 0;
		col.gridy = 1;
		col.weightx = 1.0;
		col.weighty = 1.0;
		col.insets = new Insets(20, 20, 20, 20);
		panel.add(color.getComboBoxWithoutFunctions(), col);
		
		return panel;
	}
	
	public boolean displayDialog() {
		int result = JOptionPane.showConfirmDialog(null, getPanel(), "Delete a color", JOptionPane.OK_CANCEL_OPTION,
				JOptionPane.PLAIN_MESSAGE);
		
		if (result == 0) {
			String name = "";
			int R = 0, G = 0, B = 0;
			String r, g, b;

			for (int i = 0; i < size; i++) {
				if (i == color.getSelectedIndex()) {
					name = colors.getColors().get(i).getColorname();
					R = colors.getColors().get(i).getR();
					G = colors.getColors().get(i).getG();
					B = colors.getColors().get(i).getB();
				}
			}
			r = Integer.toString(R);
			g = Integer.toString(G);
			b = Integer.toString(B);

			String string = name + "," + r + "," + g + "," + b;
			UpdateFile update = new UpdateFile();
			update.removeLineFromFile(FILENAME, string);
			
			return true;
		}
		
		return false;
	}
	
	public int getIndex() {
		return color.getSelectedIndex();
	}
}
