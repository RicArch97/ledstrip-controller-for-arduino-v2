package DailyMods;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class AddColor {
	HintTextField red, green, blue, colorName;
	private final String FILENAME = "Save files/Colors.txt";
	private RgbColor color;


	private JPanel getPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		GridBagConstraints col = new GridBagConstraints();

		JLabel name = new JLabel("Colorname:");
		name.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		col.gridx = 0;
		col.gridy = 0;
		col.weightx = 1.0;
		col.weighty = 1.0;
		col.insets = new Insets(20, 20, 20, 20);
		panel.add(name, col);

		colorName = new HintTextField("Name:");
		colorName.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		colorName.setPreferredSize(new Dimension(200, 40));
		col.gridx = 1;
		col.gridy = 0;
		col.weightx = 1.0;
		col.weighty = 1.0;
		col.insets = new Insets(20, 20, 20, 20);
		panel.add(colorName, col);

		red = new HintTextField("R:");
		col.gridx = 0;
		col.gridy = 1;
		col.weightx = 1.0;
		col.insets = new Insets(20, 20, 20, 20);
		red.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		red.setPreferredSize(new Dimension(100, 40));
		red.setDocument(new JTextFieldLimit(3));
		panel.add(red, col);

		green = new HintTextField("G:");
		col.gridx = 1;
		col.gridy = 1;
		col.weightx = 1.0;
		col.insets = new Insets(20, 20, 20, 20);
		green.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		green.setPreferredSize(new Dimension(100, 40));
		green.setDocument(new JTextFieldLimit(3));
		panel.add(green, col);

		blue = new HintTextField("B:");
		col.gridx = 2;
		col.gridy = 1;
		col.weightx = 1.0;
		col.insets = new Insets(20, 20, 20, 20);
		blue.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		blue.setPreferredSize(new Dimension(100, 40));
		blue.setDocument(new JTextFieldLimit(3));
		panel.add(blue, col);

		RGBField rgb = new RGBField();
		rgb.setPreferredSize(new Dimension(200, 40));
		rgb.setBackground(Color.BLACK);
		rgb.revalidate();
		rgb.repaint();
		col.gridx = 3;
		col.gridy = 1;
		col.weightx = 1.0;
		col.insets = new Insets(20, 20, 20, 20);
		panel.add(rgb, col);

		red.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				JTextField textField = (JTextField) e.getSource();
				try {
					if (!(textField.getText().isEmpty())) {
						if (Integer.parseInt(textField.getText()) <= 255) {
							if (green.getText().isEmpty() && blue.getText().isEmpty()) {
								rgb.setBackground(new Color(Integer.parseInt(textField.getText()), 0, 0));
								rgb.revalidate();
								rgb.repaint();
							}

							else if (green.getText().isEmpty() && (!(blue.getText().isEmpty()))) {
								rgb.setBackground(new Color(Integer.parseInt(textField.getText()), 0,
										Integer.parseInt(blue.getText())));
								rgb.revalidate();
								rgb.repaint();
							}

							else if (blue.getText().isEmpty() && (!(green.getText().isEmpty()))) {
								rgb.setBackground(new Color(Integer.parseInt(textField.getText()),
										Integer.parseInt(green.getText()), 0));
								rgb.revalidate();
								rgb.repaint();
							}

							else {
								rgb.setBackground(new Color(Integer.parseInt(textField.getText()),
										Integer.parseInt(green.getText()), Integer.parseInt(blue.getText())));
								rgb.revalidate();
								rgb.repaint();
							}
						}
					}
				} catch (Exception er) {
					textField.setText("");
				}
			}
		});

		green.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				JTextField textField = (JTextField) e.getSource();
				try {
					if (!(textField.getText().isEmpty())) {
						if (Integer.parseInt(textField.getText()) <= 255) {
							if (red.getText().isEmpty() && blue.getText().isEmpty()) {
								rgb.setBackground(new Color(0, Integer.parseInt(textField.getText()), 0));
								rgb.revalidate();
								rgb.repaint();
							}

							else if (red.getText().isEmpty() && (!(blue.getText().isEmpty()))) {
								rgb.setBackground(new Color(0, Integer.parseInt(textField.getText()),
										Integer.parseInt(blue.getText())));
								rgb.revalidate();
								rgb.repaint();
							}

							else if (blue.getText().isEmpty() && (!(red.getText().isEmpty()))) {
								rgb.setBackground(new Color(Integer.parseInt(red.getText()),
										Integer.parseInt(textField.getText()), 0));
								rgb.revalidate();
								rgb.repaint();
							}

							else {
								rgb.setBackground(new Color(Integer.parseInt(red.getText()),
										Integer.parseInt(textField.getText()), Integer.parseInt(blue.getText())));
								rgb.revalidate();
								rgb.repaint();
							}
						}
					}
				} catch (Exception er) {
					textField.setText("");
				}
			}
		});

		blue.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				JTextField textField = (JTextField) e.getSource();
				try {
					if (!(textField.getText().isEmpty())) {
						if (Integer.parseInt(textField.getText()) <= 255) {
							if (green.getText().isEmpty() && red.getText().isEmpty()) {
								rgb.setBackground(new Color(0, 0, Integer.parseInt(textField.getText())));
								rgb.revalidate();
								rgb.repaint();
							}

							else if (green.getText().isEmpty() && (!(red.getText().isEmpty()))) {
								rgb.setBackground(new Color(Integer.parseInt(red.getText()), 0,
										Integer.parseInt(textField.getText())));
								rgb.revalidate();
								rgb.repaint();
							}

							else if (red.getText().isEmpty() && (!(green.getText().isEmpty()))) {
								rgb.setBackground(new Color(0, Integer.parseInt(green.getText()),
										Integer.parseInt(textField.getText())));
								rgb.revalidate();
								rgb.repaint();
							}

							else {
								rgb.setBackground(new Color(Integer.parseInt(red.getText()),
										Integer.parseInt(green.getText()), Integer.parseInt(textField.getText())));
								rgb.revalidate();
								rgb.repaint();
							}
						}
					}
				} catch (Exception er) {
					textField.setText("");
				}
			}
		});

		return panel;
	}

	public boolean displayDialog() {
		int redValue = 0, greenValue = 0, blueValue = 0;
		String name = "unknown";

		int result = JOptionPane.showConfirmDialog(null, getPanel(), "Add a new color", JOptionPane.OK_CANCEL_OPTION,
				JOptionPane.PLAIN_MESSAGE);

		if (result == 0) {
			if (colorName.getText().isEmpty() && red.getText().isEmpty() && green.getText().isEmpty() && blue.getText().isEmpty()) {
				JOptionPane.showMessageDialog(null, "Enter RGB values", "Please enter a name and RGB values",
						JOptionPane.ERROR_MESSAGE);
			}

			else {
				if (!(red.getText().isEmpty())) {
					redValue = Integer.parseInt(red.getText());
				}

				if (!(green.getText().isEmpty())) {
					greenValue = Integer.parseInt(green.getText());
				}

				if (!(blue.getText().isEmpty())) {
					blueValue = Integer.parseInt(blue.getText());
				}
				
				if (!(colorName.getText().isEmpty())) {
					name = colorName.getText();
				}
				
				color = new RgbColor(name, redValue, greenValue, blueValue);
				WriteFile fw = new WriteFile(FILENAME);
				fw.writeFile(color);
				
				return true;
			}

		}
		return false;
	}
	
	public RgbColor getColor() {
		return color;
	}
}
