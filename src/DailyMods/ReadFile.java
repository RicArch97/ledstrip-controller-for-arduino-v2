package DailyMods;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

public class ReadFile {

	private Vector<RgbColor> RgbColors;
	private final String FILENAME;

	public ReadFile(String FILE) {
		this.FILENAME = FILE;
		
		
	}

	public void readFile() {
		RgbColors = new Vector<RgbColor>();
		BufferedReader br = null;
		FileReader fr = null;

		try {

			fr = new FileReader(FILENAME);
			br = new BufferedReader(fr);

			String line = null;

			while ((line = br.readLine()) != null) {
				String colorName = "";
				String[] values = {};
				int R = 0, G = 0, B = 0;
				
				values = line.split(",");
				colorName = values[0];
				R = Integer.parseInt(values[1]);
				G = Integer.parseInt(values[2]);
				B = Integer.parseInt(values[3]);
				
				//create color
				RgbColor color = new RgbColor(colorName, R, G, B);
				RgbColors.add(color);
			}

		} catch (IOException e) {

			e.printStackTrace();

		} finally {
			try {
				br.close();
			} catch (IOException e) {

				e.printStackTrace();
			}
		}
	}
	
	public Vector<RgbColor> getColors() {
		return RgbColors;
	}
}
