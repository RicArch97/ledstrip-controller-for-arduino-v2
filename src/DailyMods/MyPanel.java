package DailyMods;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public class MyPanel extends JComponent{
	BufferedImage img;
	
	public MyPanel() {
		try {
			img = ImageIO.read(new File("Images/Banner2.jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
    protected void paintComponent(Graphics g) {
        super.paintComponents(g);
        g.drawImage(img, 0, 0, getWidth(), getHeight(), this);
    }
}
