package DailyMods;

import java.io.BufferedReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

import com.fazecast.jSerialComm.SerialPort;

public class Devices {
	private BufferedReader input;
	private OutputStream output;
	private int index = 0;
	private String portName = "";
	private String comName = "";
	private SerialPort ArduinoPorts;
	
	public Devices(BufferedReader inp, OutputStream out, int in, String port, String com, SerialPort Ports) {
		this.input = inp;
		this.output = out;
		this.index = in;
		this.portName = port;
		this.comName = com;
		this.ArduinoPorts = Ports;
	}
	
	public String getComName() {
		return comName;
	}
	
	public int getIndex() {
		return index;
	}
	
	public void setIndex(int in) {
		this.index = in;
	}
	
	public OutputStream getOutput() {
		return output;
	}
	
	public String getDevicename() {
		return portName;
	}
	
	public SerialPort getArduinoPort() {
		return ArduinoPorts;
	}
	
	public BufferedReader getInput() {
		return input;
	}
}
