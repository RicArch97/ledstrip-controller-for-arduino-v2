package DailyMods;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

public class DynamicPanel extends JPanel {
	private final String name;
	public OutputStream output;
	public Font font;
	public JButton send, add;
	public Vector<SelectColor> sendColors;
	public GridBagConstraints col;
	public SelectColor color1, color2, color3, color4, color5, color6;
	public int colorAmount = 2;
	public ChangeSpeed speedAdjust;
	private boolean mergeColors = false;

	public DynamicPanel(String name, OutputStream output) {
		this.name = name;
		this.output = output;

		waveEffect();
	}

	@Override
	public String toString() {
		return name;
	}

	public void waveEffect() {
		initialise();
	}

	public void initialise() {
		this.setLayout(new GridBagLayout());
		col = new GridBagConstraints();
		sendColors = new Vector<SelectColor>();

		JComboBox<String> selectColorAmount = new JComboBox<String>();
		selectColorAmount.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		selectColorAmount.setPreferredSize(new Dimension(150, 40));
		selectColorAmount.addItem("2 Colors");
		selectColorAmount.addItem("3 Colors");
		selectColorAmount.addItem("4 Colors");
		selectColorAmount.addItem("5 Colors");
		selectColorAmount.addItem("6 Colors");
		selectColorAmount.addActionListener(new ChooseColorAmount());
		col.gridx = 0;
		col.gridy = 0;
		col.weightx = 1.0;
		col.weighty = 1.0;
		this.add(selectColorAmount, col);

		color1 = new SelectColor(this);
		sendColors.add(color1);
		col.gridx = 1;
		col.gridy = 0;
		col.weightx = 1.0;
		col.weighty = 1.0;
		this.add(color1.getComboBox(), col);

		color2 = new SelectColor(this);
		sendColors.add(color2);
		col.gridx = 2;
		col.gridy = 0;
		col.weightx = 1.0;
		col.weighty = 1.0;
		this.add(color2.getComboBox(), col);

		color3 = new SelectColor(this);
		sendColors.add(color3);
		col.gridx = 3;
		col.gridy = 0;
		col.weightx = 1.0;
		col.weighty = 1.0;
		this.add(color3.getComboBox(), col);

		color4 = new SelectColor(this);
		sendColors.add(color4);
		col.gridx = 1;
		col.gridy = 1;
		col.weightx = 1.0;
		col.weighty = 1.0;
		this.add(color4.getComboBox(), col);

		color5 = new SelectColor(this);
		sendColors.add(color5);
		col.gridx = 2;
		col.gridy = 1;
		col.weightx = 1.0;
		col.weighty = 1.0;
		this.add(color5.getComboBox(), col);

		color6 = new SelectColor(this);
		sendColors.add(color6);
		col.gridx = 3;
		col.gridy = 1;
		col.weightx = 1.0;
		col.weighty = 1.0;
		this.add(color6.getComboBox(), col);

		JLabel speedText = new JLabel("Fade / No fade: ");
		speedText.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		col.gridx = 0;
		col.gridy = 2;
		col.weightx = 1.0;
		col.weighty = 1.0;
		this.add(speedText, col);

		JCheckBox checkMergeColors = new JCheckBox("Merge Colors");
		col.gridx = 1;
		col.gridy = 2;
		col.weightx = 1.0;
		col.weighty = 1.0;
		checkMergeColors.setFocusPainted(false);
		checkMergeColors.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		checkMergeColors.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == 1) {
					mergeColors = true;
				} else {
					mergeColors = false;
				}
			}
		});
		this.add(checkMergeColors, col);

		send = new JButton("OK");
		send.setPreferredSize(new Dimension(100, 40));
		send.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		send.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				sendValues(colorAmount);
			}
		});
		col.gridx = 3;
		col.gridy = 2;
		col.weightx = 1.0;
		col.weighty = 1.0;
		DynamicPanel.this.add(send, col);

		selectColorAmount.setSelectedItem("2 Colors");
	}

	public void sendValues(int amount) {
		int redValue = 0, greenValue = 0, blueValue = 0;

		if (mergeColors == true) {
			sendingData((byte) 3);
		} else {
			sendingData((byte) 2);
		}
		sendingData((byte) amount);

		for (int i = 0; i < amount; i++) {
			redValue = sendColors.get(i).getValues()[0];
			greenValue = sendColors.get(i).getValues()[1];
			blueValue = sendColors.get(i).getValues()[2];

			sendingData((byte) redValue);
			sendingData((byte) greenValue);
			sendingData((byte) blueValue);
		}
	}

	public void sendingData(byte value) {
		try {
			output.write(value);
			output.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void update() {
		this.removeAll();
		this.initialise();
		this.revalidate();
		this.repaint();
	}

	class ChooseColorAmount implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			JComboBox<String> jcb = (JComboBox<String>) arg0.getSource();
			String item = jcb.getSelectedItem().toString();

			switch (item) {
			case "2 Colors":
				color1.getComboBox().setVisible(true);
				color2.getComboBox().setVisible(true);
				color3.getComboBox().setVisible(false);
				color4.getComboBox().setVisible(false);
				color5.getComboBox().setVisible(false);
				color6.getComboBox().setVisible(false);
				colorAmount = 2;
				break;
			case "3 Colors":
				color1.getComboBox().setVisible(true);
				color2.getComboBox().setVisible(true);
				color3.getComboBox().setVisible(true);
				color4.getComboBox().setVisible(false);
				color5.getComboBox().setVisible(false);
				color6.getComboBox().setVisible(false);
				colorAmount = 3;
				break;
			case "4 Colors":
				color1.getComboBox().setVisible(true);
				color2.getComboBox().setVisible(true);
				color3.getComboBox().setVisible(true);
				color4.getComboBox().setVisible(true);
				color5.getComboBox().setVisible(false);
				color6.getComboBox().setVisible(false);
				colorAmount = 4;
				break;
			case "5 Colors":
				color1.getComboBox().setVisible(true);
				color2.getComboBox().setVisible(true);
				color3.getComboBox().setVisible(true);
				color4.getComboBox().setVisible(true);
				color5.getComboBox().setVisible(true);
				color6.getComboBox().setVisible(false);
				colorAmount = 5;
				break;
			case "6 Colors":
				color1.getComboBox().setVisible(true);
				color2.getComboBox().setVisible(true);
				color3.getComboBox().setVisible(true);
				color4.getComboBox().setVisible(true);
				color5.getComboBox().setVisible(true);
				color6.getComboBox().setVisible(true);
				colorAmount = 6;
				break;
			}
		}
	}
}
