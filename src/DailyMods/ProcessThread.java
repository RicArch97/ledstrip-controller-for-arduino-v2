package DailyMods;

import javax.swing.JTabbedPane;

public class ProcessThread extends Thread {
	private ReadCOM comm;
	private JTabbedPane tabbedPane;

	public ProcessThread(ReadCOM comm, JTabbedPane tabbedPane) {
		this.comm = comm;
		this.tabbedPane = tabbedPane;
	}
	
	public void run() {

		while (!Thread.currentThread().isInterrupted()) {
			comm.checkDevices(tabbedPane);
		}
	}
}
