package DailyMods;

public class Profile {
	private String profileName;
	
	public Profile(int profileCounter)
	{
		this.profileName = "Profile" + Integer.toString(profileCounter);
	}
	
	public String getProfileName()
	{
		return profileName;
	}
}
