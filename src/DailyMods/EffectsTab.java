package DailyMods;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;
import com.sun.glass.events.MouseEvent;

public class EffectsTab extends JPanel implements SerialPortDataListener {
	JPanel uploadProfile, mainEffects;
	JTextArea uploadInstruct;
	JButton upload;
	OutputStream output;
	BufferedReader input;
	int index = 0;
	String device, deviceName, port, deviceVariable, baudRate;
	SerialPort ArduinoPort;
	JComboBox<String> Arduinos;
	String hexPath;
	String inputLine = "";
	ArrayList<Devices> dev;
	int counter, profileCounter = 1;
	JTabbedPane tabbedPane;
	JFrame frame;
	double screenWidth, screenHeight;

	public void newTab(JTabbedPane tabbedPane, ArrayList<Devices> dev, int counter, JFrame frame) {
		tabbedPane.addTab(null, null, this, null);
		this.setLayout(new BorderLayout());

		screenWidth = frame.getWidth();
		screenHeight = frame.getHeight();

		this.tabbedPane = tabbedPane;
		this.dev = dev;
		this.counter = counter;
		this.frame = frame;

		for (int i = 0; i < dev.size(); i++) {
			if (dev.get(i).getIndex() == counter) {
				deviceName = dev.get(i).getDevicename();
				port = dev.get(i).getComName();
				ArduinoPort = dev.get(i).getArduinoPort();
				output = dev.get(i).getOutput();
				input = dev.get(i).getInput();
			}
		}

		sendingData((byte) 0b01010101);

		ArduinoPort.addDataListener(this);

		uploadDefaultProfile(dev, counter, tabbedPane);
	}

	public void effectsPanel(JTabbedPane tabbedPane, ArrayList<Devices> dev, int counter) {
		mainEffects = new JPanel();
		mainEffects.setBackground(new Color(250, 250, 250));
		this.add(mainEffects, BorderLayout.CENTER);
		mainEffects.setLayout(null);

		JLabel profileText = new JLabel("PROFILE");
		profileText.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		profileText.setBounds(520, 30, 70, 30);
		mainEffects.add(profileText);

		DefaultComboBoxModel<String> model = new DefaultComboBoxModel<String>();
		JComboBox<String> Profiles = new JComboBox<String>(model);
		Profiles.setBounds(610, 30, 300, 30);
		Profiles.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		Profiles.addItem("Default (Empty)");
		mainEffects.add(Profiles);
		Profiles.addActionListener(new ActionListener() {
			private int selectedIndex = -1;

			@Override
			public void actionPerformed(ActionEvent e) {
				int index = Profiles.getSelectedIndex();
				if (index >= 0) {
					selectedIndex = index;
				}

				Profiles.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {

					@Override
					public void keyReleased(KeyEvent event) {
						if (event.getKeyChar() == KeyEvent.VK_ENTER) {
							String newValue = model.getSelectedItem().toString();
							model.removeElementAt(selectedIndex);
							model.addElement(newValue);
							Profiles.setSelectedItem(newValue);
							selectedIndex = model.getIndexOf(newValue);

							Profiles.setEditable(false);
						}
					}
				});
			}
		});

		JPopupMenu popup = new JPopupMenu();

		JMenuItem addProfile = new JMenuItem("Add");
		addProfile.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		popup.add(addProfile);
		addProfile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evv) {
				Profile newProfile = new Profile(profileCounter);
				String item = newProfile.getProfileName();
				Profiles.addItem(item);
				Profiles.setSelectedItem(item);
				profileCounter++;
			}
		});
		JMenuItem renameProfile = new JMenuItem("Rename");
		renameProfile.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		popup.add(renameProfile);
		renameProfile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ev) {
				if (!(Profiles.getSelectedItem().equals("Default (Empty)"))) {
					Profiles.setEditable(true);
				}

				else {
					Profiles.setEditable(false);
				}
			}
		});
		JMenuItem deleteProfile = new JMenuItem("Delete");
		deleteProfile.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		popup.add(deleteProfile);
		deleteProfile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent et) {
				if (!(Profiles.getSelectedItem().equals("Default (Empty)"))) {
					int dialogButton = JOptionPane.YES_NO_OPTION;
					int dialogResult = JOptionPane.showConfirmDialog(null, "Do you really want to delete this profile?",
							"Delete Profile", dialogButton);
					if (dialogResult == JOptionPane.YES_OPTION) {
						Profiles.removeItem(Profiles.getSelectedItem());
						profileCounter--;
					}
				}

				else {
					deleteProfile.setEnabled(false);
				}
			}
		});

		MenuButton profileOptions = new MenuButton("\u00B7 \u00B7 \u00B7", popup);
		profileOptions.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		profileOptions.setBackground(new Color(250, 250, 250));
		profileOptions.setBounds(930, 30, 50, 30);
		profileOptions.setFocusPainted(false);
		profileOptions.setBorderPainted(false);
		mainEffects.add(profileOptions);

		LedstripSettings ledstripSettings = new LedstripSettings(output);
		mainEffects.add(ledstripSettings);

		JPanel effectsBox = new JPanel();
		effectsBox.setBackground(new Color(240, 240, 240));
		effectsBox.setBounds(500, 220, 900, 400);
		effectsBox.setLayout(new BoxLayout(effectsBox, BoxLayout.Y_AXIS));
		mainEffects.add(effectsBox);

		JPanel effectSelections = new JPanel();
		GridBagLayout grid = new GridBagLayout();
		effectSelections.setLayout(grid);
		GridBagConstraints eff = new GridBagConstraints();
		effectsBox.add(effectSelections);

		JLabel effectText = new JLabel("Effect");
		effectText.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		effectText.setOpaque(false);
		eff.gridx = 0;
		eff.gridy = 0;
		eff.weightx = 1.0;
		effectSelections.add(effectText, eff);

		JLabel effectType = new JLabel("Default");
		effectType.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		effectType.setOpaque(false);
		eff.gridx = 1;
		eff.gridy = 0;
		eff.weightx = 1.0;
		effectSelections.add(effectType, eff);

		DefaultComboBoxModel<String> model3 = new DefaultComboBoxModel<String>();
		JComboBox<String> effectAddition = new JComboBox<String>(model3);
		effectAddition.setPreferredSize(new Dimension(180, 30));
		effectAddition.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		eff.gridx = 1;
		eff.gridy = 1;
		eff.weightx = 1.0;
		effectSelections.add(effectAddition, eff);
		
		JLabel effectBanner = new JLabel();
		effectBanner.setText("Static effect");
		effectBanner.setFont(new Font("Poor Richard", Font.PLAIN, 60));
		effectBanner.setBackground(new Color(240, 240, 240));
		effectBanner.setBounds(500, 120, 900, 100);
		mainEffects.add(effectBanner);

		JPanel colorSelections = new JPanel(new CardLayout());

		JComboBox<JPanel> effect = new JComboBox<JPanel>();
		effect.setPreferredSize(new Dimension(180, 30));
		effect.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		eff.gridx = 0;
		eff.gridy = 1;
		eff.weightx = 1.0;

		StaticPanel staticPanel = new StaticPanel("Static", output);
		DynamicPanel wavePanel = new DynamicPanel("Wave", output);
		DynamicPanel digitalPanel = new DynamicPanel("Digital", output);
		
		effect.addItem(staticPanel);
		effect.addItem(wavePanel);
		effect.addItem(digitalPanel);
		
		colorSelections.add(staticPanel, staticPanel.toString());
		colorSelections.add(wavePanel, wavePanel.toString());
		colorSelections.add(digitalPanel, digitalPanel.toString());

		effect.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JComboBox jcb = (JComboBox) e.getSource();
				CardLayout cl = (CardLayout) colorSelections.getLayout();
				
				effectBanner.setText(jcb.getSelectedItem().toString() + " effect");
				digitalPanel.update();
				wavePanel.update();
				staticPanel.update();

				cl.show(colorSelections, jcb.getSelectedItem().toString());
			}
		});

		effectSelections.add(effect, eff);
		effectsBox.add(effectSelections);
		effectsBox.add(colorSelections);
		
		frame.addWindowStateListener(new WindowStateListener() {
			public void windowStateChanged(WindowEvent arg0) {
				if ((arg0.getNewState() & Frame.NORMAL) == Frame.NORMAL) {
					double newWidth = frame.getWidth();

					profileText.setBounds((int) (newWidth / 2) - 240, 30, 70, 30);
					Profiles.setBounds((int) (newWidth / 2) - 150, 30, 300, 30);
					profileOptions.setBounds((int) (newWidth / 2) + 170, 30, 50, 30);

					ledstripSettings.setBounds(100, 120, 300, 500);
					effectsBox.setBounds(500, 220, 900, 400);
					effectBanner.setBounds(500, 120, 900, 100);
				}
				if ((arg0.getNewState() & Frame.MAXIMIZED_BOTH) == Frame.MAXIMIZED_BOTH) {
					double newWidth = frame.getWidth();
					double newHeight = frame.getHeight();

					profileText.setBounds((int) (newWidth / 2) - 240, 30, 70, 30);
					Profiles.setBounds((int) (newWidth / 2) - 150, 30, 300, 30);
					profileOptions.setBounds((int) (newWidth / 2) + 170, 30, 50, 30);

					double factorX = newWidth / screenWidth;
					double factorY = newHeight / screenHeight;

					double newx1 = factorX * 100;
					double newy1 = factorY * 120;
					double newWidth1 = factorX * 300;
					double newHeight1 = factorY * 500;
					ledstripSettings.setBounds((int) newx1, (int) newy1, (int) newWidth1, (int) newHeight1);

					double newx2 = factorX * 500;
					double newy2 = factorY * 220;
					double newWidth2 = factorX * 900;
					double newHeight2 = factorY * 400;
					effectsBox.setBounds((int) newx2, (int) newy2, (int) newWidth2, (int) newHeight2);
					
					double newx3 = factorX * 500;
					double newy3 = factorY * 120;
					double newWidth3 = factorX * 900;
					double newHeight3 = factorY * 100;
					effectBanner.setBounds((int) newx3, (int) newy3, (int) newWidth3, (int) newHeight3);
				}
			}
		});
	}

	public void uploadDefaultProfile(ArrayList<Devices> dev, int counter, JTabbedPane tabbedPane) {
		uploadProfile = new JPanel();
		uploadProfile.setLayout(new GridBagLayout());
		uploadProfile.setBackground(new Color(250, 250, 250));
		this.add(uploadProfile, BorderLayout.CENTER);

		GridBagConstraints con = new GridBagConstraints();

		uploadInstruct = new JTextArea(
				"The default profile is not found on this device. \nChoose your Arduino from the list and click OK to upload it.");
		uploadInstruct.setFont(new Font("Poor Richard", Font.PLAIN, 30));
		uploadInstruct.setBackground(new Color(250, 250, 250));
		uploadInstruct.setEditable(false);
		con.gridy = 0;
		con.gridx = 0;
		con.weighty = 0.5;
		uploadProfile.add(uploadInstruct, con);

		Arduinos = new JComboBox<String>();
		Arduinos.setPreferredSize(new Dimension(300, 50));
		Arduinos.setFont(new Font("Poor Richard", Font.PLAIN, 30));
		Arduinos.addItem("Choose model:");
		Arduinos.addItem("Arduino Nano");
		Arduinos.addItem("Arduino Uno");
		Arduinos.addItem("Arduino Mega 2560");
		con.gridx = 0;
		con.gridy = 1;
		con.weighty = 0.5;
		uploadProfile.add(Arduinos, con);

		Arduinos.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent arg0) {

				Arduinos = (JComboBox<String>) arg0.getSource();
				Object item = Arduinos.getSelectedItem();
				if (item == "Arduino Nano") {
					device = "-patmega328p";
					deviceVariable = "-carduino";
					baudRate = "-b57600";
					hexPath = "DefaultProfiles/DefaultNano.ino.hex";
				}

				if (item == "Arduino Uno") {
					device = "-patmega328p";
					deviceVariable = "-carduino";
					baudRate = "-b115200";
					hexPath = "DefaultProfiles/DefaultUno.ino.hex";
				}

				if (item == "Arduino Mega 2560") {
					device = "-patmega2560";
					deviceVariable = "-cwiring";
					baudRate = "-b115200";
					hexPath = "DefaultProfiles/DefaultMega.ino.hex";
				}
			}
		});

		upload = new JButton("Upload");
		upload.setPreferredSize(new Dimension(175, 60));
		upload.setFont(new Font("Poor Richard", Font.PLAIN, 40));
		con.gridx = 0;
		con.gridy = 3;
		con.weighty = 0.5;
		uploadProfile.add(upload, con);

		upload.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ArduinoPort.closePort();
				RunAVRCommand AVR = new RunAVRCommand(
						new String[] { "avrdude/bin/avrdude.exe", "-C" + "avrdude/etc/avrdude.conf", "-v", device,
								deviceVariable, "-P" + port, baudRate, "-D", "-Uflash:w:" + hexPath + ":i" });
				ArduinoPort.openPort();

				try {
					Thread.sleep(2500);
				} catch (InterruptedException el) {
					el.printStackTrace();
				}

				sendingData((byte) 0b01010101);
			}
		});
	}

	@Override
	public int getListeningEvents() {
		return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
	}

	@Override
	public void serialEvent(SerialPortEvent event) {
		if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE) {
			return;
		}

		try {
			if (input.ready()) {
				Thread.sleep(500);
				inputLine = input.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		if (inputLine.contains("DefaultProfileExists")) {
			effectsPanel(tabbedPane, dev, counter);
			uploadProfile.setVisible(false);
			mainEffects.setVisible(true);
		}
	}

	public void sendingData(byte data) {
		try {
			output.write(data);
			output.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}