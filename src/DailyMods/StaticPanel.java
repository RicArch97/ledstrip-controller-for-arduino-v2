package DailyMods;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.io.OutputStream;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class StaticPanel  extends JPanel{
	private final String name;
	public OutputStream output;
	public Font font;
	public SelectColor staticColor;

    public StaticPanel(String name, OutputStream output) {
        this.name = name;
        this.output = output;
        
        staticRGB();
    }

    @Override
    public String toString() {
        return name;
    }
    
    public void staticRGB()
	{	
		initialise();
	}
    
    public void initialise() {
    	this.setLayout(new GridBagLayout());
		GridBagConstraints col = new GridBagConstraints();
		
		staticColor = new SelectColor();
		col.gridx = 0;
		col.gridy = 0;
		col.weightx = 1.0;
		col.weighty = 1.0;
		this.add(staticColor.getComboBox(), col);
		
		JButton send = new JButton("OK");
		send.setPreferredSize(new Dimension(100, 40));
		send.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		col.gridx = 1;
		col.gridy = 0;
		col.weightx = 1.0;
		col.weighty = 1.0;
		this.add(send, col);
		
		send.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int redValue = 0, greenValue = 0, blueValue = 0;
				
				redValue = staticColor.getValues()[0];
				greenValue = staticColor.getValues()[1];
				blueValue = staticColor.getValues()[2];
				
				sendingData((byte) 1);
				sendingData((byte) redValue);
				sendingData((byte) greenValue);
				sendingData((byte) blueValue);
			}
		});
    }
    
    public void update() {
		this.removeAll();
		this.initialise();
		this.revalidate();
		this.repaint();
	}
    
    public void sendingData(byte value)
	{
		try {
			output.write(value);
			output.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
