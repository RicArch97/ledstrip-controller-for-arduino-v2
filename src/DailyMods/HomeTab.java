package DailyMods;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class HomeTab {
	JPanel Home;
	JLabel tab1, logo;
	ImageIcon icon;
	BufferedImage img;
	
	public HomeTab(JTabbedPane tabbedPane, Font font)
	{
		Home = new JPanel();
		tabbedPane.addTab("Home", null , Home, null);
		Home.setLayout(new BorderLayout());
		Home.setBackground(new Color(250,250,250));
		
		tab1 = new JLabel();
		
		tab1.setText("Home");
		tab1.setForeground(Color.BLACK);
		tab1.setFont(font.deriveFont(Font.PLAIN, 50f));
		tabbedPane.setTabComponentAt(0, tab1);
		
		try {
            img = ImageIO.read(new File("Images/Logo.png"));
        } catch(IOException e) {
            e.printStackTrace();
        }
		
		icon = new ImageIcon(img);
		logo = new JLabel(icon);
		
		Home.add(logo, BorderLayout.NORTH);
		logo.setBorder(new EmptyBorder(50, 0, 0, 0));
	}
}
