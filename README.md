# Ledstrip Controller For Arduino v2 (unfinished)

This is version 2 of a ledstrip controller for Arduino's.
The focus in this version is to give the user full control over the effects, amount of colours and the colours themselves.

# Functionality

*  Connect an Arduino to the PC. The software currently supports 5 connected Arduino's simultaneously.
*  The software automatically detects a connection to the serial port from an Arduino.
*  The software will do a detection of the required software on the Arduino. If it's not installed, a window will be shown where you can upload it.
*  If the required software is present on the Arduino, the program will preceed to show the window with the effect options.
*  On the left of the effects window, you can set the amount of leds on your ledstrip to make sure the effects are shown correctly. Also there's an option to change the speed of moving effects.
*  On the right of the effects window, you can select the effect type. The default menu shows premade effects.
*  The software allows for own made colours. Simple open the list of colours and click "add". A window will pop of to enter a name and RGB values.
*  To delete a colour, click "delete" and select the colour from the list.

# Screenshots

![screenshot1](/uploads/1cb7ea85f45f657b5a78d3beb37a7cf3/screenshot1.PNG)

![Schermopname__28_](/uploads/333cbab983f339eb4e3437a984b4b391/Schermopname__28_.png)

![Schermopname__29_](/uploads/84cf5e8e3bcb2e1a8aaf89c83ec6ced5/Schermopname__29_.png)

![Schermopname__30_](/uploads/439c430ea1229a57fac532d910191769/Schermopname__30_.png)

![Schermopname__31_](/uploads/e72b386428ba2d61190b6ff0dda7be42/Schermopname__31_.png)

# Video

https://youtu.be/4wLJcN1IVkE

# To do

*  Profiles, saving current effect + colour combo.
*  Run software on windows startup, and run the selected profile.
*  Add more effects.
